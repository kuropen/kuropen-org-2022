#!/usr/bin/env zx

const { Cdprojekt, Fsecure } = require("@icons-pack/react-simple-icons")

const API_URL = 'https://cms2.kuropen.org/api/posts?pagination[pageSize]=100&sort[]=date'

const apiResponse = await fetch(API_URL)
const allData = await apiResponse.json()

const tocMdText = `---
title: Penguinoneブログアーカイブ
penguinoneArchive: true
penguinoneArchiveIndex: true
---
Penguinoneブログは2022年XX月XX日をもちましてサービスを終了いたしました。
同日以降の記事は、Hey Worldに掲載しております。

なお、Penguinoneブログに掲載していた記事は、下記よりお読みになれます。

` + (
    allData.data.map((data) => {
        const {title, slug, date} = data.attributes
        const dateObj = new Date(date)
        const datePresentation = new Intl.DateTimeFormat('ja-JP').format(dateObj)
        echo`Added to TOC: ${slug}`

        return `- [${title} (${datePresentation})](/pgn-archives/${slug})`
    }).join("\n")
)

const promises = allData.data.map((data) => {
    const {title, slug, body, date} = data.attributes
    echo`Writing: ${slug}`
    const mdText = `---
title: ${title}
penguinoneArchive: true
date: ${date}
---
${body}`
    return fs.outputFile(`${__dirname}/../src/pages/pgn-archives/${slug}.md`, mdText)
})

promises.push(fs.outputFile(`${__dirname}/../src/pages/pgn-archives/index.md`, tocMdText))

await Promise.all(promises)
