import * as qs from "qs"
import { StrapiData, StrapiResult } from "../@types/strapiResult"

const API_URL = 'https://cms2.kuropen.org/api/portfolios' as const

const FetchDataFromStrapi = async () => {
    const requestUrl = new URL(API_URL)

    let allData: StrapiData[] = []
    let page = 1
    let hasNextPage = true
    do {
        requestUrl.search = qs.stringify({
            pagination: {
                page: page
            },
            sort: ['priority'],
        }, {
            encodeValuesOnly: true,
        })
        const apiResponse = await fetch(requestUrl.toString())
        const result: StrapiResult = await apiResponse.json()
        allData = allData.concat(result.data)

        hasNextPage = (result.meta.pagination.pageCount > page)
        page++
    } while (hasNextPage)

    return allData
}

export default FetchDataFromStrapi
