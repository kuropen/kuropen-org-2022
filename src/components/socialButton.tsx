import * as React from "react"

type SocialIconProps = {
    children: React.ReactNode
    href: string
    rel?: string
    target?: string
    customClass?: string
    title?: string
}

const DEFAULT_CLASS_DEF = "block rounded-full p-2 shadow-md bg-primary text-baseColor"

const SocialIcon = (props: SocialIconProps) => {
    let appliedClasses: string[]
    const defaultClasses = DEFAULT_CLASS_DEF.split(' ')
    if (props.customClass) {
        const classes = props.customClass.split(' ')
    
        appliedClasses = defaultClasses.map((cls) => {
            console.log(`cls: ${cls}`)
            if (cls.indexOf('-') === -1) {
                return cls
            }
            const currentClassKind = cls.split('-')[0]
            const sameKindProp = classes.find((elem) => {
                const targetKind = elem.split('-')[0]
                return currentClassKind === targetKind
            })
            console.log(`sameKindProp: ${sameKindProp}`)
            if (sameKindProp === undefined) {
                return cls
            }
            return sameKindProp
        })
        console.log(appliedClasses)
    } else {
        appliedClasses = defaultClasses
    }
    console.log(appliedClasses)
    return (<a className={appliedClasses.join(' ')} {...props} />)
}

export default SocialIcon
