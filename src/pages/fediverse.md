---
title: "Fediverse Accounts"
---
# List of Fediverse accounts (Fediverseアカウント一覧)

## Currently used (現在運用中)
- Fedibird: [@kuropen@fedibird.com](https://fedibird.com/@kuropen)

## Currently suspended (現在休止中)
- Gingadon: `@kuropen@gingadon.com`
- Machitodon: `@kuropen@matitodon.com`
- mstdn.jp: `@kuropen@mstdn.jp`

If you had followed these accounts, you are supposed to be following the Fedibird account with Mastodon's function.
(If your server uses a system other than Mastodon, you may have to manually migrate.)

あなたが上記アカウントのいずれかをフォローしていた場合、Mastodonの「引っ越し機能」により現在はFedibirdアカウントをフォローいただいています。
(Mastodon以外のシステムの場合は、手動でフォローしていただく必要がある場合があります。)

## Abolished (廃止)
- `@kuropen@akabe.co`
- `@kuropen@don.akabe.co`
- `@krpn@kuropen.me`
- `@kuropen@misskey.io`

These accounts are abolished because the servers hosted them are abolished, or rollbacked due to system failure.

これらのアカウントは、アカウントが所在するサーバーが廃止されたか、または障害の影響によりロールバックされたため、廃止されています。
