import { StrapiData } from "../@types/strapiResult"
import FetchDataFromStrapi from "../lib/fetchDataFromStrapi"
// Don't forget "index.js" below, otherwise build fails
import { CakeIcon, LocationMarkerIcon, SwitchHorizontalIcon, BriefcaseIcon, AcademicCapIcon, ExternalLinkIcon, ChevronRightIcon } from "@heroicons/react/outline/index.js"
import { Amazonaws, Android, Docker, Javascript, Laravel, Linux, Php, Typescript } from "@icons-pack/react-simple-icons"
import { GetStaticData } from "minista"

type PageHomeProps = {
    data?: StrapiData[]
}

const SKILL_ICON_SIZE = 48 as const

const PageHome = (props: PageHomeProps) => {
    return (
        <div className="grid gap-2 mx-2 md:mx-0">
            <section className="border border-primary rounded-md p-2">
                <h2 className="text-2xl mb-2">Profile</h2>
                <dl className="grid grid-cols-1 md:grid-cols-2 gap-2">
                    <div>
                        <dt className="float-left mr-2"><CakeIcon className="w-5" /><span className="sr-only">Birthday</span></dt>
                        <dd>August 3, 1989</dd>
                    </div>
                    <div>
                        <dt className="float-left mr-2"><BriefcaseIcon className="w-5" /><span className="sr-only">Occupation</span></dt>
                        <dd>Web Engineer</dd>
                    </div>
                    <div>
                        <dt className="float-left mr-2"><LocationMarkerIcon className="w-5" /><span className="sr-only">Place</span></dt>
                        <dd>Saitama, Japan <SwitchHorizontalIcon className="w-3 inline" /><span className="sr-only">and</span> Aizu-Wakamatsu, Japan</dd>
                    </div>
                    <div>
                        <dt className="float-left mr-2"><AcademicCapIcon className="w-5" /><span className="sr-only">Alma mater</span></dt>
                        <dd>The University of Aizu</dd>
                    </div>
                </dl>
            </section>
            <section className="border border-primary rounded-md p-2">
                <h2 className="text-2xl mb-2">Skills &amp; Experiences</h2>
                <div>
                    <ul className="grid grid-cols-4 md:grid-cols-5 gap-3">
                        <li className="flex flex-col items-center gap-1">
                            <div><Php size={SKILL_ICON_SIZE} /></div>
                            <div>PHP</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Laravel size={SKILL_ICON_SIZE} /></div>
                            <div>Laravel</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Javascript size={SKILL_ICON_SIZE} /></div>
                            <div>JavaScript</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Typescript size={SKILL_ICON_SIZE} /></div>
                            <div>TypeScript</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Linux size={SKILL_ICON_SIZE} /></div>
                            <div>Linux</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Docker size={SKILL_ICON_SIZE} /></div>
                            <div>Docker</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Amazonaws size={SKILL_ICON_SIZE} /></div>
                            <div>AWS</div>
                        </li>
                        <li className="flex flex-col items-center gap-1">
                            <div><Android size={SKILL_ICON_SIZE} /></div>
                            <div>Android</div>
                        </li>
                    </ul>
                </div>
            </section>
            <section className="border border-primary rounded-md p-2">
                <h2 className="text-2xl mb-2">Blog (Japanese Only)</h2>
                <nav className="grid gap-2 mx-2">
                    <a href="https://note.com/penguinote" target="_blank">
                        <div className="border rounded-md shadow-md bg-baseColor p-2 flex flex-row items-center mx-1 md:mx-0">
                            <div className="flex-grow">
                                <div className="text-xl">
                                    note
                                </div>
                                <div>
                                    Articles since October 2022
                                </div>
                            </div>
                            <div>
                                <ExternalLinkIcon className="h-6 w-6" />
                            </div>
                        </div>
                    </a>
                    <a href="/pgn-archives/">
                        <div className="border rounded-md shadow-md bg-baseColor p-2 flex flex-row items-center mx-1 md:mx-0">
                            <div className="flex-grow">
                                <div className="text-xl">
                                    Penguinone Blog Archive
                                </div>
                                <div>
                                    Articles before August 2022
                                </div>
                            </div>
                            <div>
                                <ChevronRightIcon className="h-6 w-6" />
                            </div>
                        </div>
                    </a>
                </nav>
            </section>
            <section className="border border-primary rounded-md p-2">
                <h2 className="text-2xl mb-2">Portfolio</h2>
                <nav className="grid gap-2 mx-2">
                {
                    props.data?.map((item) => (
                        <div key={item.id}>
                            <a href={item.attributes.url} target={item.attributes.newWindow ? '_blank' : '_self'}>
                                <div className="border rounded-md shadow-md bg-baseColor p-2 flex flex-row items-center mx-1 md:mx-0">
                                    <div className="flex-grow">
                                        <div className="text-xl">
                                            {item.attributes.title}
                                        </div>
                                        <div>
                                            {item.attributes.description}
                                        </div>
                                    </div>
                                    <div>
                                        {item.attributes.newWindow ? (
                                            <ExternalLinkIcon className="h-6 w-6" />
                                        ) : (
                                            <ChevronRightIcon className="h-6 w-6" />
                                        )}
                                    </div>
                                </div>
                            </a>
                        </div>
                    ))
                }
                </nav>
            </section>
            <section className="border border-primary rounded-md p-2">
                <h2 className="text-2xl mb-2">Policies</h2>
                <nav className="grid gap-2 mx-2">
                    <a href="/privacy">
                        <div className="border rounded-md shadow-md bg-baseColor p-2 flex flex-row items-center mx-1 md:mx-0">
                            <div className="flex-grow">
                                <div className="text-xl">
                                    Privacy Policy
                                </div>
                            </div>
                            <div>
                                <ChevronRightIcon className="h-6 w-6" />
                            </div>
                        </div>
                    </a>
                    <a href="https://social.kuropen.org/" target="_blank">
                        <div className="border rounded-md shadow-md bg-baseColor p-2 flex flex-row items-center mx-1 md:mx-0">
                            <div className="flex-grow">
                                <div className="text-xl">
                                    SNS Policy (Japanese Only)
                                </div>
                            </div>
                            <div>
                                <ExternalLinkIcon className="h-6 w-6" />
                            </div>
                        </div>
                    </a>
                </nav>
            </section>
            <section className="border border-primary rounded-md p-2">
                This website is generated with <a href="https://minista.qranoko.jp/" className="underline" target="_blank">minista</a> {/**/}
                and served via <a href="https://pages.cloudflare.com/" className="underline" target="_blank">Cloudflare Pages</a>.<br />
                Icons used in this website are <a href="https://simpleicons.org/" className="underline" target="_blank">Simple Icons</a> and {/**/}
                <a href="https://heroicons.com/" className="underline" target="_blank">Heroicons</a>.<br />
                <a href="https://www.php.net/download-logos.php" className="underline" target="_blank">PHP logo</a> by Colin Viebrock is licensed under {/**/}
                <a href="https://creativecommons.org/licenses/by-sa/4.0/" className="underline" target="_blank">Creative Commons Attribution-ShareAlike 4.0 International</a>.<br />
                <a href="https://gitlab.com/kuropen/kuropen-org-2022" className="underline" target="_blank">Source code for this website</a>
            </section>
        </div>
    )
}

export default PageHome

export const getStaticData: GetStaticData = async () => {
    const data = await FetchDataFromStrapi()
    return {
        props: {
            data: data
        }
    }
}
