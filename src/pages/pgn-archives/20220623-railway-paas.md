---
title: Railwayを試す
penguinoneArchive: true
date: 2022-06-22T22:30:00.000Z
---
[Railway](https://railway.app/)というPaaSがある。GitHubからの自動デプロイや、MySQL, PostgreSQL, Redisといったデータベースエンジンを一括管理できるという。

難点としてはデータセンターがアメリカにしかないこととされるが、昨今の情勢から[アメリカでのサーバー構築を許容した](/posts/20220621-servers-on-usa-due-to-weak-yen)筆者にとってはもはや問題にならない。ということで試してみた。

## 料金プラン

[料金プラン](https://railway.app/pricing)を見てみると、分間で使用したCPUとメモリに対して料金が設定されており、無償プランは月間5ドルまで相当分を使用することができる。一方、有償プランであっても月間10ドルまでの無償枠が設定されており、かなり利用料は安い。

## Vercelからの移行

Vercelは個人が無償で利用することができるが、広告の掲載は禁止されている。筆者運営の[akabe.co](http://akabe.co)サイトは赤べこに関連する商品を紹介するコーナーがあるが、厳密にはVercelに置いたままこれを掲載してしまうことは問題であったため、転出することにした。

すでにVercelで利用しているリポジトリを選択すると自動でデプロイが始まり、すんなりと稼働状態に持っていくことができた。

なお、VercelはCDNとしてVercelを使うことを要求しているが、RailwayについてはCloudflare CDNの裏側に置いても問題がない。

## Strapi CMSの移行

このブログをリニューアルするにあたって、記事のGitHub管理をやめるために導入したヘッドレスCMSのStrapiもついでに移行することにした。こうすることによってインフラコストをさらに抑制することができるためである。

しかし一つ問題があり、StrapiはMySQL 8.0の暗号化パスワードに対応していない。AWSやGCPのマネージドDBは、たとえMySQL 8.0を選んでも暗号化パスワードに対応する必要はないが、Railwayは[公式Dockerコンテナ](https://hub.docker.com/_/mysql)をデプロイする方式のためか、提供されるユーザーの暗号化パスワードが有効になっている。

ただ、その提供されるユーザーは `root` であったため、別途 `IDENTIFIED WITH mysql_native_password` が指定されたユーザーを作成することによって、Strapiをデプロイしても問題がなくなった。

なお、RailwayはCLIを提供しており、CLIのコマンドで簡単にMySQLのクライアントを起動することができるが、CLIが入っているマシンに、MySQL 8系またはMariaDB 10系のクライアントがインストールされている必要がある。（5系の場合は暗号化パスワードの関係でログインできないと思われる。）

## 構成図

以下がシステム構成図の変化である。だいぶシンプルになった。

### 移行前

![システム構成図（移行前）](https://imagedelivery.net/AlvvY7HhQXA6AbbJDoG41A/21b848e9-4435-485e-56f8-6be679cf5200/public) 

### 移行後
![システム構成図（移行後）    ](https://imagedelivery.net/AlvvY7HhQXA6AbbJDoG41A/0525cf52-6ea5-42b7-1ba3-3b8d65294b00/public) 
