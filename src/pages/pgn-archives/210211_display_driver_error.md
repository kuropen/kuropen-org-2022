---
title: ディスプレイドライバエラー多発を受けた対応
penguinoneArchive: true
date: 2021-02-10T15:00:00.000Z
---
![Cover Image](https://imagedelivery.net/AlvvY7HhQXA6AbbJDoG41A/429ca550-cc89-4322-ef41-3f6e9fecde00/public)

【2021年2月17日追記】

本件は、[NVIDIAの修正版ドライバ公開](https://www.nichepcgamer.com/archives/geforce-driver-461-51-hotfix.html)により解決しております。

大変お騒がせ致しました。

-------------------

現在使用しているメインPCにおいて、ChromeとMicrosoft Edgeを使っていると突然ディスプレイが数秒間ブラックアウトし、システムログで
ディスプレイ ドライバー nvlddmkm が応答を停止しましたが、正常に回復しました。
と記載される例が発生しています。


原因ははっきりしていませんが、SNSを見たところでは、NVIDIA GeForce RTX 3000 シリーズのグラフィックボードを搭載したPCで同様の報告が相次いでいることから、NVIDIAのディスプレイドライバーとChromium系ブラウザの間に相性問題が発生しているものと考えられます。

マルチディスプレイ環境に起因しているとの情報があるため、サブディスプレイを撤去して問題回避を試みていますが、これで問題が回避できない場合は、Windows PCのGoogle ChromeとMicrosoft Edgeにおける当サイトの動作確認をとりやめる場合がございます。

何卒ご理解とご協力をお願いいたします。
