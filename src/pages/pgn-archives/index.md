---
title: Penguinoneブログアーカイブ
penguinoneArchive: true
penguinoneArchiveIndex: true
---
[Penguinoneブログ](https://penguinone.kuropen.org/)は2022年10月1日をもちましてサービスを終了します。
2022年10月以降の記事は、[note](https://note.com/penguinote)に掲載しております。

なお、Penguinoneブログに掲載していた記事は、下記よりお読みになれます。

- [異常事態、列車の外に出るのは正解か？ (2018/1/20)](/pgn-archives/6fe9f918-4a1e-456b-b4e4-907ac64d1c54)
- [社会の中で成長する (2018/1/29)](/pgn-archives/7510b626-4662-460c-b966-854c16717244)
- [2018年度初めの雑感 (2018/4/2)](/pgn-archives/97cd0a3c-aaa1-4af7-ad0f-c113aa63041e)
- [埼京線ダイヤ改正所感 (2019/12/1)](/pgn-archives/0344f788-e48d-4d52-8c2f-a40e3ae0192b)
- [健康祈願 (2020/2/3)](/pgn-archives/200203-pray-for-health)
- [「人が読めればいい」という紙文化 (2020/5/13)](/pgn-archives/20200513_human_only_recognize)
- [浮き草になる人々 (2020/5/17)](/pgn-archives/20200517_people_who_have_multiple_home)
- [「事後孔明」に気を付けよう (2020/5/22)](/pgn-archives/20200522_monday_morning_quaterback)
- [福がある残り物 (2020/8/13)](/pgn-archives/20200813-good-luck-remains)
- [ドライバーとしての心得 (2020/8/22)](/pgn-archives/20200822_moral_of_drivers)
- [ 私の携帯電話遍歴 (2020/9/8)](/pgn-archives/20200908_my_mobilephones)
- [node.js開発にはWSL1 (2020/12/30)](/pgn-archives/20201230_wsl1_for_node)
- [2021年になりました (2021/1/1)](/pgn-archives/2021_new_year_card)
- [NetlifyからAWSに移管しました (2021/2/5)](/pgn-archives/210205_migrate_to_aws)
- [キーボードを新調 (2021/2/5)](/pgn-archives/210205_logicool_g610)
- [ディスプレイドライバエラー多発を受けた対応 (2021/2/11)](/pgn-archives/210211_display_driver_error)
- [「あの日」から10年 (2021/3/13)](/pgn-archives/20210313-10-years-from-that-day)
- [PCデスクの整備完了 (2021/4/2)](/pgn-archives/20210402-pc-desk)
- [ブラウザをBraveに乗り換えました (2021/4/4)](/pgn-archives/20210404-brave-browser)
- [知らないと知られていないことがある (2021/4/14)](/pgn-archives/20210414-unknown-unknowns)
- [急にiPhoneでの表示チェックが必要になったとき (2021/5/18)](/pgn-archives/20210518-for-suddenly-needs-of-iphone)
- [賽は投げられた (2021/6/12)](/pgn-archives/20210612-alea-jacta-est)
- [サイトの見直しなど (2021/7/19)](/pgn-archives/20210719-restructuring-my-web)
- [なんでやっちゃダメなの？ (2021/7/27)](/pgn-archives/20210727-why-i-should-not-do)
- [Nitterを試してみた (2021/8/27)](/pgn-archives/20210827_nitter)
- [「電力の今」プロジェクト再始動 (2021/9/8)](/pgn-archives/20210908_japan-electricity-dashboard)
- [ActivityPub (Dolphin) 個人インスタンスに関するご報告 (2021/9/16)](/pgn-archives/20210916-notice-regarding-dolphin)
- [新型コロナウイルスワクチン接種1回目 (2021/9/24)](/pgn-archives/20210924-covid-19-vaccination-1st)
- [ActivityPubアカウントに関するご報告(10/19追記) (2021/10/19)](/pgn-archives/20211005-notice-regarding-misskey)
- [iOSでWebアプリのデバッグログを見る (2021/10/21)](/pgn-archives/20211021-console-log-ios)
- [新型コロナウイルスワクチン接種2回目 (2021/11/3)](/pgn-archives/20211103-covid-19-vaccination-2nd)
- [アップグレードの準備ができました（できるとは言っていない）（12/3追記） (2021/11/13)](/pgn-archives/20211113-upgrade-is-ready-but-unable)
- [Cloudflare Pagesからの移動(12/26追記・訂正あり) (2021/12/26)](/pgn-archives/20210922-moving-from-cloudflare)
- [2022年になりました (2022/1/1)](/pgn-archives/20220101-new-year-card)
- [プライバシーと実用性 (2022/4/10)](/pgn-archives/20220410-privacy-and-usability)
- [円安"だから"アメリカにサーバーを建てる (2022/6/21)](/pgn-archives/20220621-servers-on-usa-due-to-weak-yen)
- [Railwayを試す (2022/6/23)](/pgn-archives/20220623-railway-paas)
- [Mastodonアカウントの移転統合について (2022/6/25)](/pgn-archives/20220625-notice-mastodon-account)
- [GitLabリポジトリをRailwayにデプロイする (2022/7/7)](/pgn-archives/20220707-deploy-gitlab-repo-to-railway)
- [社会問題とキャンセルカルチャー (2022/8/31)](/pgn-archives/20220831-social-issues-and-cancelling)