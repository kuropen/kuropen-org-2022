---
title: iOSでWebアプリのデバッグログを見る
penguinoneArchive: true
date: 2021-10-21T12:05:00.000Z
---
Macを持たない[^1]者にとって、WebアプリであってもiOSでの動作確認はかなりハードルが高い問題です。

しかし先日リリースされた iOS 15 でブラウザアドオンが拡充されたようで、
[実機画面で開発者ツールを表示できるアドオン](https://apps.apple.com/jp/app/web-inspector/id1584825745)が登場していました。

![iOS Safariで開発者ツールを表示した状態](https://imagedelivery.net/AlvvY7HhQXA6AbbJDoG41A/5811c569-dcf4-4815-308b-9df320521500/public)

DOMツリーやconsole.logもきちんと表示できています。

Webアプリの実機検証が少し楽になる気がしました。

[^1]: かつては持っていましたが今は手放しています。
