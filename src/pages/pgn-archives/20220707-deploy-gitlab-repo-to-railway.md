---
title: GitLabリポジトリをRailwayにデプロイする
penguinoneArchive: true
date: 2022-07-07T10:42:00.000Z
---
AIコーディング支援サービスに関する諸問題 から、[GitHubへの批判が高まっています](https://gigazine.net/news/20220704-software-freedom-conservancy-give-up-github/)。

私としても何かあった時の備えとして、いくつかの制作物のコードをGitLabに移転しましたが、[Railway](/posts/20220623-railway-paas)はGitLab連携には現在のところ対応していません。

しかし、RailwayはGitリポジトリではなくコマンドラインインターフェイスを使った直接デプロイができ、それを GitLab CI に組み込むことにより、GitLabリポジトリと連携ができます。

## 準備
1. デプロイ対象となるプロジェクトを作成する。なお、2つ以上のアプリケーションコンテナを一つのプロジェクトに同居させた場合は、CIによる自動デプロイはできないようである。
2. [プロジェクトトークン](https://docs.railway.app/deploy/integrations) を発行する。
3. GitLabのプロジェクトの CI/CD 設定で、環境変数 `RAILWAY_TOKEN` に上記プロジェクトトークンを設定する。

## .gitlab-ci.yml
下記内容の `.gitlab-ci.yml` を作成すれば Railway にデプロイができます。

```
default:
  image: 'alpine:latest'
  before_script:
    - apk add curl tar
    - curl -fsSL https://railway.app/install.sh | sh

stages:
  - deploy

deploy to production:
  stage: deploy
  script: railway up
  environment:
    name: production
  rules:
    - if: '$CI_COMMIT_BRANCH == "main"'
```
