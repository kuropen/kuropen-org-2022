---
title: アップグレードの準備ができました（できるとは言っていない）（12/3追記）
penguinoneArchive: true
date: 2021-11-13T02:28:00.000Z
---
自作PCにWindows 11へのアップグレード通知が11月2日に来ていますが、
いざアップグレードを実行しようとすると、 `0xc1900101` のSTOPエラーが出て起動せず、Windows 10に自動的にロールバックされます。

自動的にロールバックされるということでまだ安心感はありますが、このSTOPエラーはデバイスドライバの問題ということで、
この状態でアップグレード準備完了という表示がWindows Updateに残り続けることには違和感があります。

![残り続けているアップグレード通知](https://imagedelivery.net/AlvvY7HhQXA6AbbJDoG41A/79d0fefb-743e-4c3a-5a41-adcd0c232200/public)

自作して1年のマシンですが、そもそも第3世代CoreのゲーミングPCの緊急リプレイスを目的に作ったもので、
グラフィックボード以外のパーツは相当低予算で作っていることを考えると、
グラフィックボードだけ引き継いだ新しいマシン作る方がコストがかからないのかなあ。

## 追記
上記のアップグレードが妨害される事象は、PowerDVDが原因との情報を入手し、PowerDVDを削除してアップグレードを行ったところ、アップグレードに成功しました。

※[Fix INVALID DATA ACCESS TRAP error on Windows 11](https://www.thewindowsclub.com/fix-invalid-data-access-trap-error-on-windows)において、STOPエラーでWindows 10にロールバックされた場合の確認事項として **Uninstall Media related apps like PowerDVD** との記載がある。
