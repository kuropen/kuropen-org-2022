import { MinistaLocation } from "minista"
import SocialButton from "./components/socialButton"
import "./styles/main.css"

import { Head } from "minista"

import { Twitter, Facebook, Github, Mastodon } from "@icons-pack/react-simple-icons"
import ChevronLeftIcon from "@heroicons/react/outline/ChevronLeftIcon.js"
import React from "react"
import DateBox from "./components/dateBox"

type FrontmatterProps = {
  title?: string
  penguinoneArchive?: boolean
  penguinoneArchiveIndex?: boolean
  date?: string
}

type RootProps = {
  location: MinistaLocation
  frontmatter?: FrontmatterProps
  children: React.ReactNode
}

const SOCIAL_ICON_SIZE = 24 as const
const PenguinImg = 'https://imagedelivery.net/AlvvY7HhQXA6AbbJDoG41A/2d20aa0e-f89b-4375-59aa-183093b17f00/icon' as const

const Root = ({ frontmatter, children }: RootProps) => {
  return (
    <>
        <Head>
            <title>{frontmatter?.title ? `${frontmatter.title} - ` : ''}Kuropen</title>
            <link rel="icon" href={PenguinImg} />
            <link rel="me" href="https://fedibird.com/@kuropen" />
        </Head>
        <div className="container max-w-screen-md mx-auto">
            <header className="flex flex-col md:flex-row my-2 pb-2 items-center">
                <div className="mb-2 md:mb-0">
                    <a href="/">
                        <div className="flex flex-row items-center justify-center">
                            <img src={PenguinImg} alt="" className="w-12 h-12 rounded-full mr-2 border" />
                            <h1 className="text-3xl">Kuropen</h1>
                        </div>
                    </a>
                </div>
                <div className="hidden grow md:block"></div>
                <ul className="flex flex-row justify-center md:justify-end gap-x-2">
                    <li><SocialButton href="/fediverse"><Mastodon size={SOCIAL_ICON_SIZE} /><span className="sr-only">Mastodon</span></SocialButton></li>
                    <li><SocialButton href="https://twitter.com/kuropen_aizu" target="_blank" customClass="bg-gray-400"><Twitter size={SOCIAL_ICON_SIZE} /><span className="sr-only">Twitter</span></SocialButton></li>
                    <li><SocialButton href="https://facebook.com/yuda.hirochika" target="_blank"><Facebook size={SOCIAL_ICON_SIZE} /><span className="sr-only">Facebook</span></SocialButton></li>
                    <li><SocialButton href="https://github.com/kuropen" target="_blank"><Github size={SOCIAL_ICON_SIZE} /><span className="sr-only">GitHub</span></SocialButton></li>
                </ul>
            </header>
            <main>
                {frontmatter ? (
                    <>
                        {
                            frontmatter.penguinoneArchive ? (
                                <section className="border rounded-md shadow-md p-2 bg-primary text-baseColor">
                                    <h2 className="text-2xl">{frontmatter.title}</h2>
                                    {
                                        frontmatter.date ? (
                                            <div><DateBox date={frontmatter.date} /></div>
                                        ) : (<React.Fragment />)
                                    }
                                </section>
                            ) : (
                                <React.Fragment />
                            )
                        }
                        <section className="border rounded-md shadow-md p-2 my-2">
                            <div className="prose mx-auto">
                                {children}
                            </div>
                        </section>
                        <nav>
                            {
                                frontmatter.penguinoneArchive && !frontmatter.penguinoneArchiveIndex ? (
                                    <a href="/pgn-archives/" className="flex flex-row gap-2 items-center justify-center bg-primary text-baseColor rounded-md shadow-md my-2 p-2 mx-1 md:mx-0">
                                        <div><ChevronLeftIcon className="w-8 h-8" /></div>
                                        <div>Penguinoneブログアーカイブへ戻る</div>
                                    </a>
                                ) : (<React.Fragment />)
                            }
                            <a href="/" className="flex flex-row gap-2 items-center justify-center bg-primary text-baseColor rounded-md shadow-md my-2 p-2 mx-1 md:mx-0">
                                <div><ChevronLeftIcon className="w-8 h-8" /></div>
                                <div>Return to the Top Page</div>
                            </a>
                        </nav>
                    </>
                ) : (
                    <div>
                        {children}
                    </div>
                )}
            </main>
            <footer className="mt-4 mb-2 pt-2">
                <address className="text-right">All rights reserved. Copyright (C) Kuropen.</address>
            </footer>
        </div>
    </>
  )
}

export default Root
