type PenguinoneArchiveToc = {
    title: string
    slug: string
    date: string
}[]

export default PenguinoneArchiveToc
