export type PenguinonePortfolioAttribute = {
    title: string
    url: string
    description: string
    priority?: number
    newWindow?: boolean
    createdAt: string
    updatedAt: string
    publishedAt: string
}

export type StrapiData = {
    id: number
    attributes: PenguinonePortfolioAttribute
}

export type StrapiPagination = {
    page: number
    pageSize: number
    pageCount: number
    total: number
}

export type StrapiMeta = {
    pagination: StrapiPagination
}

export type StrapiResult = {
    data: Array<StrapiData>
    meta: StrapiMeta
}
